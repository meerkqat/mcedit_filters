from random import randint
from random import choice
from copy import deepcopy
from pymclevel import TAG_Compound
from pymclevel import TAG_Int
from pymclevel import TAG_Short
from pymclevel import TAG_Byte
from pymclevel import TAG_String
from pymclevel import TAG_Float
from pymclevel import TAG_Double
from pymclevel import TAG_List

# Shuffles the inventories of all the chests in the selected area.
# First it pools all the items in the chests into an array and then equally distributes random items to each chest.

displayName = "Shuffle chests"

inputs = (
	("Keep stacks", True),
	("Randomize item placement", True),
	("D", 0),
)

def perform(level, box, options):
	keep_stacks = options["Keep stacks"]
	rand_items = options["Randomize item placement"]
	deviation = options["D"]
	chests = []
	inventory = []
	
	for pos_x in range(box.minx, box.maxx):
		for pos_y in range(box.miny, box.maxy):
			for pos_z in range(box.minz, box.maxz):
			
				if (level.blockAt(pos_x, pos_y, pos_z) == 54):
					#get chest
					chest = level.tileEntityAt(pos_x, pos_y, pos_z)
					if chest == None: #chests placed with mcedit don't have a tile entity
						continue
					chests.append(chest)
					
					#get inventory of chest
					for item in chest["Items"]:
						if keep_stacks:
							item = [item]
						else:
							item = breakStack(item)
						inventory += item
						
					#clear chest inventory
					chest["Items"] = TAG_List()
					
					chunk = level.getChunk(pos_x/16, pos_z/16)
					chunk.dirty = True
	
	items_per_chest = len(inventory)/len(chests)
	
	i = 0
	while len(inventory) > 0:
		chest = chests[i]
		item = choice(inventory)
		inventory.remove(item)
		
		(slot, add_only) = findSlot(item, chest, rand_items)
		
		if add_only:
			for it in chest["Items"]:
				if it["Slot"].value == slot:
					#print "item", item["Count"], item["id"]
					#print "it", it["Count"], it["id"]
					it["Count"] = TAG_Byte(it["Count"].value+item["Count"].value)
					#print "it", it["Count"], it["id"]
					#print "adding ",item["Count"], item["id"], chest["x"]
					break
		else:
			item["Slot"] = TAG_Byte(slot)
			chest["Items"].append(item)
			#print "appending ",item["Count"], item["id"], chest["x"]
			
		i += 1
		if i == len(chests):
			i = 0

# breaks a stack of items into an array of single items			
def breakStack(item):
	items = []
	n = item["Count"].value
	item["Count"] = TAG_Byte(1)
	for i in range(n):
		items.append(deepcopy(item))
		
	return items
	
def findSlot(item, chest, rand):
	taken_slots = []
	id_count = []
	for it in chest["Items"]:
		taken_slots.append(it["Slot"].value)
		id_count.append((it["id"].value, it["Count"].value))
	
	slot = -1
	while slot == -1:
		if rand:
			slot = randint(0,26)
		else:
			slot += 1
		
		try: #an item already occupies this slot
			i = taken_slots.index(slot)
			if id_count[i][0] == item["id"].value and id_count[i][1]+item["Count"].value <= 64:
				return (slot, True)
			else:
				slot = -1
		except: #slot is empty
			return (slot, False)
		