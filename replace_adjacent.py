from pymclevel import alphaMaterials

# Replaces all blocks of the specified kind that are adjacent to blocks specified by "Next to"
# By default it only replaces blocks that share a face with a "Next to" block
# With "Check diagonals" enabled it will replace the block if it shares a side or a face with a "Next to" block
# Likewise with "Check corners" enabled it will replace blocks that share a face side or corner (so all the blocks that surround it)

displayName = "Replace adjacent"

inputs = (
	("Replace", alphaMaterials.Glowstone),
	("Next to", alphaMaterials.Air),
	("With", alphaMaterials.Glass),
	("Check diagonals", False),
	("Check corners", False),
)

def perform(level, box, options):
	block = options["Replace"]
	adj = options["Next to"]
	rep = options["With"]
	diagonals = options["Check diagonals"]
	corners = options["Check corners"]
	
	for x in range(box.minx, box.maxx):
		for y in range(box.miny, box.maxy):
			for z in range(box.minz, box.maxz):
				if (level.blockAt(x,y,z) == block.ID and level.blockDataAt(x,y,z) == block.blockData):
					if (level.blockAt(x+1,y,z) == adj.ID and level.blockDataAt(x+1,y,z) == adj.blockData) \
					or (level.blockAt(x-1,y,z) == adj.ID and level.blockDataAt(x-1,y,z) == adj.blockData) \
					or (level.blockAt(x,y+1,z) == adj.ID and level.blockDataAt(x,y+1,z) == adj.blockData) \
					or (level.blockAt(x,y-1,z) == adj.ID and level.blockDataAt(x,y-1,z) == adj.blockData) \
					or (level.blockAt(x,y,z+1) == adj.ID and level.blockDataAt(x,y,z+1) == adj.blockData) \
					or (level.blockAt(x,y,z-1) == adj.ID and level.blockDataAt(x,y,z-1) == adj.blockData):
						level.setBlockAt(x, y, z, rep.ID)
						level.setBlockDataAt(x, y, z, rep.blockData)
						chunk = level.getChunk(x/16, z/16)
						chunk.dirty = True
						continue
						
					if diagonals:
						if (level.blockAt(x+1,y+1,z) == adj.ID and level.blockDataAt(x+1,y+1,z) == adj.blockData) \
						or (level.blockAt(x,y+1,z+1) == adj.ID and level.blockDataAt(x,y+1,z+1) == adj.blockData) \
						or (level.blockAt(x+1,y,z+1) == adj.ID and level.blockDataAt(x+1,y,z+1) == adj.blockData) \
						or (level.blockAt(x-1,y-1,z) == adj.ID and level.blockDataAt(x-1,y-1,z) == adj.blockData) \
						or (level.blockAt(x,y-1,z-1) == adj.ID and level.blockDataAt(x,y-1,z-1) == adj.blockData) \
						or (level.blockAt(x-1,y,z-1) == adj.ID and level.blockDataAt(x-1,y,z-1) == adj.blockData) \
						or (level.blockAt(x+1,y-1,z) == adj.ID and level.blockDataAt(x+1,y-1,z) == adj.blockData) \
						or (level.blockAt(x,y+1,z-1) == adj.ID and level.blockDataAt(x,y+1,z-1) == adj.blockData) \
						or (level.blockAt(x+1,y,z-1) == adj.ID and level.blockDataAt(x+1,y,z-1) == adj.blockData) \
						or (level.blockAt(x-1,y+1,z) == adj.ID and level.blockDataAt(x-1,y+1,z) == adj.blockData) \
						or (level.blockAt(x,y-1,z+1) == adj.ID and level.blockDataAt(x,y-1,z+1) == adj.blockData) \
						or (level.blockAt(x-1,y,z+1) == adj.ID and level.blockDataAt(x-1,y,z+1) == adj.blockData):
							level.setBlockAt(x, y, z, rep.ID)
							level.setBlockDataAt(x, y, z, rep.blockData)
							chunk = level.getChunk(x/16, z/16)
							chunk.dirty = True
							continue
							
					if corners:
						if (level.blockAt(x+1,y+1,z+1) == adj.ID and level.blockDataAt(x+1,y+1,z+1) == adj.blockData) \
						or (level.blockAt(x-1,y-1,z-1) == adj.ID and level.blockDataAt(x-1,y-1,z-1) == adj.blockData) \
						or (level.blockAt(x+1,y-1,z-1) == adj.ID and level.blockDataAt(x+1,y-1,z-1) == adj.blockData) \
						or (level.blockAt(x-1,y+1,z+1) == adj.ID and level.blockDataAt(x-1,y+1,z+1) == adj.blockData) \
						or (level.blockAt(x+1,y+1,z-1) == adj.ID and level.blockDataAt(x+1,y+1,z-1) == adj.blockData) \
						or (level.blockAt(x-1,y-1,z+1) == adj.ID and level.blockDataAt(x-1,y-1,z+1) == adj.blockData) \
						or (level.blockAt(x+1,y-1,z+1) == adj.ID and level.blockDataAt(x+1,y-1,z+1) == adj.blockData) \
						or (level.blockAt(x-1,y+1,z-1) == adj.ID and level.blockDataAt(x-1,y+1,z-1) == adj.blockData):
							level.setBlockAt(x, y, z, rep.ID)
							level.setBlockDataAt(x, y, z, rep.blockData)
							chunk = level.getChunk(x/16, z/16)
							chunk.dirty = True
							