These are a bunch of Python filters I wrote for MCEdit. Some are more useful than 
others and some are just for plain fun and shenanigans.
All of them should have at least a brief description of what they do at the 
beginning of the script.