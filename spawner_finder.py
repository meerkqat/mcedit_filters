# Displays coordinates of all spawners within the selection box.
# Can output to file (useful for larger selections) or just to console.

displayName = "Spawner Finder"

inputs = (
	("Just print coordinates of selection to console", False),
	("Output file:", ("string","value=spawners")),
	("Origin x:", 0),
	("Origin y:", 0),
	("Origin z:", 0),
)

def perform(level, box, options):
	pcoords = options["Just print coordinates of selection to console"]
	outf = options["Output file:"]
	ox = options["Origin x:"]
	oy = options["Origin y:"]
	oz = options["Origin z:"]

	if pcoords:
		print "x: "+str(box.minx)+"-"+str(box.maxx)
		print "y: "+str(box.miny)+"-"+str(box.maxy)
		print "z: "+str(box.minz)+"-"+str(box.maxz)
		return
	
	spawners = []
	for x in xrange(box.minx, box.maxx):
		for y in xrange(box.miny, box.maxy):
			for z in xrange(box.minz, box.maxz):
				b = level.blockAt(x, y, z)
				if b == 52:
					spawners.append((x,y,z))

	f = open(outf+".txt", 'w')
	for (x,y,z) in spawners:
		d = abs(x-ox)+abs(y-oy)+abs(z-oz)
		line = "("+str(x)+","+str(y)+","+str(z)+") ; "+str(d)
		f.write(line+"\n")
		print line
	f.close()