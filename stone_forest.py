from pymclevel import alphaMaterials

# Replaces (oak) trees with another material. 
# By default converts logs into stone and leaves into cobblestone

displayName = "Stone Forest"

inputs = (
	("Leaves:", alphaMaterials.Cobblestone),
	("Trunk:", alphaMaterials.Stone),
)

def perform(level, box, options):
	leaf = options["Leaves:"]
	trunk = options["Trunk:"]
	
	for x in xrange(box.minx, box.maxx):
		for y in xrange(box.miny, box.maxy):
			for z in xrange(box.minz, box.maxz):
				b = level.blockAt(box.minx + x, box.miny + y, box.minz + z)
				if(b == 17): #trunk
					level.setBlockAt(box.minx + x, box.miny + y, box.minz + z, trunk.ID)
					level.setBlockDataAt(box.minx + x, box.miny + y, box.minz + z, trunk.blockData)					
				elif(b == 18): #leaves
					level.setBlockAt(box.minx + x, box.miny + y, box.minz + z, leaf.ID)
					level.setBlockDataAt(box.minx + x, box.miny + y, box.minz + z, leaf.blockData)
					
	chunk.chunkChanged()