# Removes all water (still and flowing) from selected area
# You can specify above which level the water should be removed - default is 63 which is sea level.

displayName = "Dehydrate"

inputs = (
	("Above level", (63, 0, 1000)),
)

def perform(level, box, options):
	lv = options["Above level"]

	for y in range(lv, box.maxy):
		for x in range(box.minx, box.maxx):
			for z in range(box.minz, box.maxz):
				if (level.blockAt(x,y,z) == 8 or level.blockAt(x,y,z) == 9):
					level.setBlockAt(x,y,z,0)
					level.getChunk(x / 16, z / 16).dirty = True
					