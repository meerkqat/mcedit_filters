from math import cos, ceil
from copy import copy, deepcopy
from pymclevel import alphaMaterials

"""
Script for generating hilly & mountanious terrain in MCEdit.
Options:
	Top layer 						- material	- Type of blocks on top.
	Foundation 						- material 	- Type of blocks at the bottom.
	Depth of top layer 				- int 		- Number of top layer blocks in a column.
	Persistance 					- float 	- Usually set between 0 & 1, affects how high/low the terrain will be. 
													Higher values mean more mountains, though it will also dig into the ground more - 
													it's basically the terrain amplitude.
	Additional elevation 			- int 		- At higher levels of persistence you might want to have additional elevation, so you 
													don't dig into the ground (too much).
	Chunkiness 						- int 		- Smaller values of chunkinesswill produce more needle like towers, while larger values 
													will make more plateau or plains type terrain.
	Iterations of smoothing 		- int 		- More iterations means less blocky & more natural looking terrain, though lag will probably 
													occur on higher values. Uses Gaussian blur to smooth out terrain.
	Random seed 					- int 		- Can be any interger. Is involved in picking prime numbers from the array of primes bellow. 
	Delete blocks on overlap 		- bool 		- If enabled, terrain generation only effects blocks that are specified by always_replace array - 
													the new terrain will "drape" itself on the old one.
	Limit height to selection box 	- bool 		- If enabled, terrain will not be generated above the selection box.
	Limit depth to selection box 	- bool 		- If enabled, terrain will not be generated below the selection box 
													(a simple way to prevent digging into the ground).
	
Besides all the options, terrain generation is also affected by the height of the selection box and
the position of the selection box on the x & z axis - i.e. an nxnxn sized selection box will generally form different terrain in different places. 

Also note that changes made outside of the selection box cannot be undone with Ctrl+Z or "Undo".
"""

# primes used for random noise function
primes = [50021, 50023, 50033, 50047, 50051, 50053, 50069 
, 50077, 50087, 50093, 50101, 50111, 50119, 50123, 50129, 50131, 50147 
, 50153
, 51131, 51133, 51137, 51151, 51157, 51169, 51193, 51197, 51199, 51203 
, 51217, 51229, 51239, 51241, 51257, 51263, 51283, 51287, 51307, 51329 
, 51341, 51343, 51347, 51349, 51361, 51383, 51407, 51413, 51419, 51421 
, 51427
, 57107, 57119, 57131, 57139, 57143, 57149, 57163, 57173, 57179, 57191 
, 57193, 57203, 57221, 57223
, 62467, 62473, 62477, 62483, 62497, 62501, 62507, 62533, 62539, 62549 
, 62563, 62581, 62591, 62597, 62603, 62617, 62627, 62633, 62639, 62653 
, 62659, 62683, 62687, 62701, 62723, 62731, 62743, 62753, 62761, 62773 
, 62791
, 64951, 64969, 64997, 65003, 65011, 65027, 65029, 65033, 65053, 65063 
, 65071, 65089, 65099, 65101, 65111, 65119, 65123, 65129, 65141, 65147 
, 65167, 65171, 65173, 65179, 65183, 65203, 65213, 65239, 65257, 65267 
, 65269, 65287, 65293, 65309, 65323, 65327, 65353, 65357, 65371, 65381 
, 65393
, 68219, 68227, 68239, 68261, 68279, 68281, 68311, 68329, 68351, 68371 
, 68389, 68399, 68437, 68443, 68447, 68449, 68473, 68477, 68483, 68489 
, 68491, 68501, 68507, 68521, 68531, 68539, 68543, 68567, 68581, 68597 
, 68611, 68633, 68639, 68659, 68669, 68683, 68687, 68699, 68711, 68713 
, 68729
, 72251, 72253, 72269, 72271, 72277, 72287, 72307, 72313, 72337, 72341 
, 72353, 72367, 72379, 72383, 72421, 72431, 72461, 72467, 72469, 72481 
, 72493, 72497, 72503, 72533, 72547, 72551, 72559, 72577, 72613, 72617 
, 72623, 72643, 72647, 72649
, 79301, 79309, 79319, 79333, 79337, 79349, 79357, 79367, 79379, 79393 
, 79397, 79399, 79411, 79423, 79427, 79433, 79451, 79481, 79493, 79531 
, 79537, 79549, 79559, 79561, 79579, 79589, 79601, 79609, 79613, 79621 
, 79627, 79631, 79633, 79657, 80233, 80239, 80251, 80263 
, 80273, 80279, 80287, 80309, 80317, 80329, 80341, 80347, 80363, 80369 
, 80387, 80407, 80429, 80447, 80449, 80471, 80473, 80489, 80491, 80513 
, 80527, 80537, 80557, 80567, 80599, 80603, 80611, 80621, 80627, 80629 
, 80651
, 82903, 82913, 82939, 82963, 82981, 82997, 83003, 83009, 83023, 83047 
, 83059, 83063, 83071, 83077, 83089, 83093, 83101, 83117, 83137, 83177 
, 83203, 83207, 83219, 83221, 83227, 83231, 83233, 83243, 83257, 83267 
, 83269, 83273, 83299, 83311, 83339, 83341, 83357, 83383, 83389, 83399 
, 83401, 83407, 83417, 83423, 83431, 83437, 83443, 83449, 83459, 83471 
, 83477, 83497, 83537, 83557, 83561, 83563, 83579, 83591, 83597, 83609 
, 83617, 83621, 83639
, 90863, 90887, 90901, 90907, 90911, 90917, 90931, 90947, 90971, 90977 
, 90989, 90997, 91009, 91019, 91033, 91079, 91081, 91097, 91099, 91121 
, 91127, 91129, 91139, 91141, 91151, 91153, 91159, 91163, 91183, 91193 
, 91199, 91229, 91237, 91243, 91249, 91253, 91283, 91291, 91297, 91303 
, 91309, 91331, 91367, 91369, 91373, 91381, 91387, 91393, 91397, 91411 
, 91423, 91433, 91453, 91457, 91459, 91463, 91493, 91499, 91513, 91529 
, 91541, 91571, 91573, 91577, 91583, 91591, 91621, 91631, 91639, 91673 
, 91691, 91703, 91711, 91733, 91753, 91757, 91771, 91781, 91801, 91807 
, 91811, 91813, 91823, 91837, 91841, 91867, 91873, 91909, 91921, 91939 
, 91943, 91951, 91957
, 97919, 97927, 97931, 97943, 97961, 97967, 97973, 97987, 98009, 98011 
, 98017, 98041, 98047, 98057, 98081, 98101, 98123, 98129, 98143, 98179 
, 98207, 98213, 98221, 98227, 98251, 98257, 98269, 98297, 98299, 98317 
, 98321, 98323, 98327, 98347, 98369, 98377, 98387, 98389, 98407, 98411 
, 98419, 98429, 98443, 98453, 99377, 99391, 99397 
, 99401, 99409, 99431, 99439, 99469, 99487, 99497, 99523, 99527, 99529 
, 99551, 99559, 99563, 99571, 99577, 99581, 99607, 99611, 99623, 99643 
, 99661, 99667, 99679, 99689, 99707, 99709, 99713, 99719, 99721, 99733 
, 99761, 99767, 99787, 99793, 99809, 99817, 99823, 99829, 99833, 99839 
, 99859, 99871, 99877, 99881, 99901, 99907, 99923, 99929, 99961, 99971 
, 99989, 99991]

def cosine_interpolate(a, b, x): #float a, float b, float x
	ft = x * 3.1415927
	f = (1 - cos(ft)) * 0.5

	return  a*(1-f) + b*f

def gaussian_blur(noise, le, lei): #int[][] noise, int le, int lei
	#0.0030    0.0133    0.0219    0.0133    0.0030
    #0.0133    0.0596    0.0983    0.0596    0.0133
    #0.0219    0.0983    0.1621    0.0983    0.0219
    #0.0133    0.0596    0.0983    0.0596    0.0133
    #0.0030    0.0133    0.0219    0.0133    0.0030
	ret = []
	for y in range(2,le-2):
		ret.append([])
		for x in range(2,lei-2):
			val  = 0.0030*noise[y-2][x-2]+0.0133*noise[y-2][x-1]+0.0219*noise[y-2][x]+0.0133*noise[y][x+1]  +0.0030*noise[y-2][x+2]
			val += 0.0133*noise[y-1][x-2]+0.0596*noise[y-1][x-1]+0.0983*noise[y-1][x]+0.0596*noise[y][x+1]  +0.0133*noise[y-1][x+2]
			val += 0.0219*noise[y][x-2]  +0.0983*noise[y][x-1]  +0.1621*noise[y][x]  +0.0983*noise[y][x+1]  +0.0219*noise[y][x+2]
			val += 0.0133*noise[y+1][x-2]+0.0596*noise[y+1][x-1]+0.0983*noise[y+1][x]+0.0596*noise[y+1][x+1]+0.0133*noise[y+1][x+2]
			val += 0.0030*noise[y+2][x-2]+0.0133*noise[y+2][x-1]+0.0219*noise[y+2][x]+0.0133*noise[y+2][x+1]+0.0030*noise[y+2][x+2]
			ret[-1].append(int(ceil(val)))

	return ret
	
def noise(x, y, index, seed): #int x, int y, int seed
	n = x + y * 57
	n = (n<<13) ^ n

	# %534 = %len(primes)
	return ( 1.0 - ( (n * (n * n * primes[(index*seed)%534] + primes[index+(17*seed)%534]) + 1376312589) & 0x7fffffff) / 1073741824.0)

def smooth_noise(x, y, index, seed): # float x, float y, int seed
	corners = (noise(x-1, y-1, index, seed)+noise(x+1, y-1, index, seed)+noise(x-1, y+1, index, seed)+noise(x+1, y+1, index, seed)) / 16
	sides   = (noise(x-1, y, index, seed)  +noise(x+1, y, index, seed)  +noise(x, y-1, index, seed)  +noise(x, y+1, index, seed)) /  8
	center  =  noise(x, y, index, seed) / 4
	
	return corners + sides + center

def interpolated_noise(x, y, index, seed): # float x, float y, int seed
	int_x    = int(x)
	fract_x = x - int_x

	int_y    = int(y)
	fract_y = y - int_y

	v1 = smooth_noise(int_x,     int_y, index, seed)
	v2 = smooth_noise(int_x + 1, int_y, index, seed)
	v3 = smooth_noise(int_x,     int_y + 1, index, seed)
	v4 = smooth_noise(int_x + 1, int_y + 1, index, seed)

	i1 = cosine_interpolate(v1 , v2 , fract_x)
	i2 = cosine_interpolate(v3 , v4 , fract_x)

	return cosine_interpolate(i1 , i2 , fract_y)

def perlin_noise(x,y, p, n, seed): # float x, float y, float persistence, int numOctaves-1, int seed
	total = 0
	for i in range(n):
		frequency = 2*i
		amplitude = p*i
		total = total + interpolated_noise(x * frequency, y * frequency, i, seed) * amplitude
	
	return total


displayName = "Mountain Generator"

inputs = (
	("Top layer:", alphaMaterials.Grass),
	("Foundation:", alphaMaterials.Stone),
	("Depth of top layer:", (3,0,100)),
	("Persistance:",(0.25,0.0,100.0)),
	#("Number of octaves:",(3,1,100)),
	("Additional elevation:",(4,0,100)),
	("Chunkiness:", (2,1,100)),
	("Iterations of smoothing:",(1, 0, 100)),
	("Random seed:", 1),
	("Delete blocks on overlap:", True),
	("Limit height to selection box:", True),
	("Limit depth to selection box:", True),
)

def perform(level, box, options):
	top_layer = options["Top layer:"]
	top_layer_depth = options["Depth of top layer:"]
	foundation = options["Foundation:"]
	persistance = options["Persistance:"]
	#num_octaves = options["Number of octaves:"] # more or less redundant, changing this gives similar results to changing persistency
	additional_elevation = options["Additional elevation:"]
	chunkiness = options["Chunkiness:"]
	smooth_iter = options["Iterations of smoothing:"]
	seed = options["Random seed:"]
	delete_on_overlap = options["Delete blocks on overlap:"]
	height_limit = options["Limit height to selection box:"]
	depth_limit = options["Limit depth to selection box:"]

	#these blocks are always replaced regardless of delete_on_overlap (0 -air- should always be included)
	always_replace = [0,31,32,43] 
	
	max_elevation = box.maxy - box.miny
	width = (box.maxx - box.minx)/chunkiness
	length = (box.maxz - box.minz)/chunkiness
	noise = []
	num_octaves = 5
	
	# perlin noise
	# box.maxx+box.minx adds some locality randomness - a selection box of the same size 
	# will generally not generate the same terrain if the box isn't placed in the same spot
	for i in range(box.maxx+box.minx, box.maxx+box.minx+width): 
		noise.append([])
		for j in range(box.maxz+box.minz, box.maxz+box.minz+length):
		
			val = int(ceil(perlin_noise(i,j,persistance, num_octaves, seed)*10+additional_elevation))
			for s in range(chunkiness):
				noise[-1].append(copy(val))
				
		for s in range(chunkiness-1):
			noise.append(deepcopy(noise[-1]))

	
	#fix matrix to fit box size
	while(len(noise[0]) < (box.maxz - box.minz)):
		for w in range(len(noise)):
			noise[w].append(copy(noise[w][-1]))

	while(len(noise) < (box.maxx - box.minx)):
		noise.append(deepcopy(noise[-1]))
	
	le = len(noise)
	lei = len(noise[0])
	
	z_iter = range(lei)
	x_iter = range(le)
	
	# smoothing
	if (smooth_iter > 0):
		le+=4
		lei+=4
	for smooth in range(smooth_iter):
		# first make a bounding box of zeroes
		# TODO: make bounding box the elevations of blocks surrounding the selection box
		#	-> better results when smoothing (perhaps less seams with the surrounding area)
		prep0 = []
		app0 = []
		
		for i in range(lei):
			prep0.append(0)
			app0.append(0)
			
		for e in noise:
			e.insert(0,0)
			e.insert(0,0)
			e.append(0)
			e.append(0)

		noise.insert(0, prep0)
		noise.insert(0, prep0)
		noise.append(app0)
		noise.append(app0)
		
		
		noise = gaussian_blur(noise, le, lei)
		
		#x_iter = range(le-4)
		#z_iter = range(lei-4)

	# mountain creation
	for x in x_iter:
		for z in z_iter:
			h = noise[x][z]
			
			if(depth_limit and h <= 0):
				for y in range(max_elevation): # foreach block above ground; delete
					level.setBlockAt(box.minx + x, box.miny + y, box.minz + z, 0)
					level.setBlockDataAt(box.minx + x, box.miny + y, box.minz + z, 0)
				continue
				
			for y in range(h): # foreach block in column
				if (height_limit and y >= max_elevation): # reached height limit of selection box (if enabled)
					break
				b = level.blockAt(box.minx + x, box.miny + y, box.minz + z)
				if (not delete_on_overlap and b not in always_replace): # delet on overlap not enabled & current block in not to be replaced
					pass
				elif(y >= (h-top_layer_depth)): # generate top layer
					level.setBlockAt(box.minx + x, box.miny + y, box.minz + z, top_layer.ID)
					level.setBlockDataAt(box.minx + x, box.miny + y, box.minz + z, top_layer.blockData)
				else: # generate foundation
					level.setBlockAt(box.minx + x, box.miny + y, box.minz + z, foundation.ID)
					level.setBlockDataAt(box.minx + x, box.miny + y, box.minz + z, foundation.blockData)

			for y in range(h,max_elevation): # foreach block above column; delete
				level.setBlockAt(box.minx + x, box.miny + y, box.minz + z, 0)
				level.setBlockDataAt(box.minx + x, box.miny + y, box.minz + z, 0)
	
	#chunk changed
	for chunk, slices, point in level.getChunkSlices(box):				
		chunk.chunkChanged()
