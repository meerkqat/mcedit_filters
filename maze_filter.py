"""
" NOTE: This bit is about how the maze generation works, info more specifically about the filter can be found about half way down 
" the file (search for "MCEdit stuffs").
"
" The maze algorithm builds an abstract representation of a 2D maze, based on an array of nodes. Uses depth-first search with backtracking.
" [ N N N N ...
"   N N N N ...
"   . . . . .
"   . . . .  .
"   . . . .   .]
" 
" The maze extrapolated from this array would look like this:
" W ? W ? W ? W ? W ...
" ? N ? N ? N ? N ? ...
" W ? W ? W ? W ? W ...
" ? N ? N ? N ? N ? ...
" W ? W ? W ? W ? W ...
" . . . . . . . . . .
" . . . . . . . . .  .
" . . . . . . . . .   .
" 	Sidenote: the dimensions of the node array should be at least 1x1, the resulting maze will then always be QxM, where Q & M are odd numbers.
" 
" W denotes a wall
" N denotes an array element (empty space in maze)
" ? can be a wall or empty space (passage), depending on what value the adjacent Ns are (the ones touching it)
" 
" 
" Node format:
" N is a number between 0b0001 and 0b1111.
" 	Sidenote: for simplicity lets say N is a 4 bit int and the rightmost bit is the first bit and the leftmost one is the fourth bit.
" 
" Each bit in N represents a direction (up, down, left or right) where an adjacent passage or wall can be (the ? on the maze above).
" If the bit is set to 0, that direction contains a wall, if it's set to 1, it contains a passage.
" The order of directions is as follows:
"   2
" 1 N 3
"   4
" 
" Example:
" If we have a node with a value 0b1100, then it will have walls to the left and up, and passages to the right and down.
" i.e.
" W W W
" W N _
" W _ W
"
"""

from random import seed
from random import choice
from random import randint
from random import random
from bisect import bisect
from pymclevel.materials import alphaMaterials

# -----------------
# 	Core stuffs
# -----------------

VALID_DIRECTIONS = [0b0001, 0b0010, 0b0100, 0b1000]
REVERSE_DIRECTION = {0b0001:0b0100, 0b0010:0b1000, 0b0100:0b0001, 0b1000:0b0010}

# Finds which nodes adjacent to node_array[x][y] have not yet been visited (the nodes that you can go to in the next step). 
#
# @param 	x : int ; x coordinate in node_array
# 		 	y : int ; y coordinate in node_array
# 		 	node_array : [int][int] ; the 2D array of nodes that represents the maze
# @return 	neighborhood : int ; "4-bit binary" number denoting which neighbours have not yet been visited 
#  									(format as explained above). ; 0 = free, 1 = visited
def get_free_neighbours(x, y, node_array):
	neighborhood = 0
	
	# Check for bounds of course
	if (x > 0):
		neighborhood |= 0b0001 if (node_array[y][x-1] == 0) else 0b0000
	if (y > 0):
		neighborhood |= 0b0010 if (node_array[y-1][x] == 0) else 0b0000
	if (x < len(node_array[0])-1):
		neighborhood |= 0b0100 if (node_array[y][x+1] == 0) else 0b0000
	if (y < len(node_array)-1):
		neighborhood |= 0b1000 if (node_array[y+1][x] == 0) else 0b0000

	return neighborhood

# Determines the number of possible valid connections a node at coordinates x,y can make with its neighbours. 
# Values range from 0 to 4
#
# @param 	x : int ; x coordinate in node_array
# 		 	y : int ; y coordinate in node_array
# 			node_array : [int][int] ; the 2D array of nodes that represents the maze
# @return 	n : int ; number of possible valid connections (number between 0 and 4)
def num_unused_connections(x, y, node_array):
	n = 0
	node = node_array[y][x]

	if (x > 0 and (node & 0b0001) == 0):
		n += 1
	if (x < len(node_array[0]) -1 and (node & 0b0100) == 0):
		n += 1
	if (y > 0 and (node & 0b0010) == 0):
		n += 1
	if (y < len(node_array)-1 and (node & 0b1000) == 0):
		n += 1

	return n

# Determines the new x and y coordinates after making one move, given the starting x and y, and N direction
#
# @param 	x : int ; starting x coordinate in node_array
# 			y : int ; starting y coordinate in node_array
# 			direction : int ; direction of move
# @return 	(x,y) : (int,int) ; the new (moved) x and y coordinates in node_array
def move(x, y, direction):
	if (direction == VALID_DIRECTIONS[0]):
		x = x-1
	elif (direction == VALID_DIRECTIONS[1]):
		y = y-1
	elif (direction == VALID_DIRECTIONS[2]):
		x = x+1
	elif (direction == VALID_DIRECTIONS[3]):
		y = y+1

	return (x,y)

# Builds the node_array that represents the maze
#
# @param 	maze_width : int ; the width of the node array (note that the actual maze is then 2*maze_width+1 wide)
# 		 	maze_height : int ; the height of the node array (note that the actual maze is then 2*maze_height+1 high)
# 		 	num_rnd_passages : int ; number of random passages to put in the maze/walls to break after the initial 
#										generation (initially the algorithm constructs the node_array as a tree graph 
# 										and this puts in some cycles)
# 		 	x : int ; x coordinate where the algorithm will start the depth-first search/maze generation 
# 						if value is -1, it will pick a random x
# 		 	y : int ; y coordinate where the algorithm will start the depth-first search/maze generation 
# 						if value is -1, it will pick a random y
# @return 	node_array : [int][int] ; array of nodes that can be extrapolated into a maze
def build_maze(maze_width, maze_height, num_rnd_passages, x=0, y=0):
	# For quick reference
	#  node_array dimensions:
	#   [[ , ...],  |
	#    [ , ...],  | maze_height  inc
	#    [ , ...],  |    = y        |
	#         ...]  |               V
	#
	#   ---------
	#    maze_width = x
	# 		inc ->
	#
	#  node_array access:
	#   node_array[y][x]
	#
	# Both node_array and node_array_backtrack are the same dimensions and in the same format, the difference is 
	# node_array is a bidirectional graph and node_array_backtrack isn't (it only has pointers to where you came from)
	node_array = [[0 for w in range(maze_width)] for h in range(maze_height)]
	node_array_backtrack = [[0 for w in range(maze_width)] for h in range(maze_height)]

	if (x == -1):
		x = randint(1,maze_width) -1
	elif (x >= maze_width):
		x = maze_width-1

	if (y == -1):
		y = randint(1,maze_height)-1
	elif (y >= maze_height):
		y = maze_height-1

	num_unvisited = maze_height*maze_width - 1
	while (num_unvisited > 0):
		neighborhood = get_free_neighbours(x,y,node_array)

		if neighborhood != 0:
			# go in random direction

			direction = choice(VALID_DIRECTIONS)
			# make sure it's possible to go in selected direction (going towards empty node)
			while (neighborhood & direction not in VALID_DIRECTIONS):
				direction = choice(VALID_DIRECTIONS)

			node_array[y][x] |= direction
			x,y = move(x, y, direction)
			node_array[y][x] = REVERSE_DIRECTION[direction]
			node_array_backtrack[y][x] = REVERSE_DIRECTION[direction]

			num_unvisited = num_unvisited - 1

		else:
			# backtrack one

			direction = node_array_backtrack[y][x]
			tail = REVERSE_DIRECTION[direction]
			x,y = move(x, y, direction)

	# add random passages (break walls)
	for i in range(num_rnd_passages):
		x = randint(1,maze_width)-1
		y = randint(1,maze_height)-1

		# make sure it's possible to make new (valid) connection for this node
		# this can get stuck if num_rnd_passages is too large, so try to get a new node only a certain number of times
		j = 0
		while (num_unused_connections(x,y,node_array) == 0 and j < 20):
			x = randint(1,maze_width)-1
			y = randint(1,maze_height)-1
			j += 1
		if (j == 20):
			continue

		direction = choice(VALID_DIRECTIONS)

		# make sure you don't break the outer wall (border cases)
		# or try to make a path where there already is one (connect two connected nodes)
		while ( (x == 0 and direction == 0b0001) or \
				(y == 0 and direction == 0b0010) or \
				(x == maze_width -1 and direction == 0b0100) or \
				(y == maze_height-1 and direction == 0b1000) or 
				node_array[y][x] & direction != 0 ):
			direction = choice(VALID_DIRECTIONS)

		# connect nodes
		node_array[y][x] |= direction
		x,y = move(x, y, direction)
		node_array[y][x] |= REVERSE_DIRECTION[direction]

	return node_array

# -----------------
# 	MCEdit stuffs
# -----------------

# Maze dimensions are defined by the bounding box. For this to work the selection must be at least 3x3, and 4 high, if the maze 
# has a ceiling (otherwise at least 2 high).
# Obviously the larger the selection the more intricate the maze can become, however, at very large scales 
# it will probably take some time to generate. 
# Also try experimenting with the number of random passages, if the maze is small, a large number of these will make 
# the maze very open and not very maze-like. In the opposite "extreme" case where random passages are set to 0, the maze will
# be kinda like a tree graph and won't contain any cycles/loops.

displayName = "Maze Generator"

# Four material types per wall/floor/ceiling not enough? here's how to add more:
# Just add the appropriate option in the inputs array and then you have to read the option like it's done
# around line 340 and everything else should be automatic.
# e.g. add a fifth material to the walls:
# add 
# ("Wall material 5", alphaMaterials.Stone),
# ("Wall material 5 weight: ", 5),
# below into inputs array - right after it says ("Wall material 4 weight: ", 5),
# then set that option in the wall_materials array by appending
# (options["Wall material 5"],options["Wall material 5 weight: "])
# in wall_materials (around line 335)
inputs = [
	(
	("After the initial generation the maze will look like a tree graph (no cycles), this option breaks random walls to create loops in the graph", "label"),
	("Number of random passages", (5,0,500)),
	("If x & y are set to -1, maze generation will start from a random point in the selection","label"),
	("Starting x:", (0, -1, 500)),
	("Starting y:", (0, -1, 500)),
	("Leave seed blank for Python default","label"),
	("Random seed:", ("string","value=""")),
	("General", "title"),
	),
	(
	("Wall material 1", alphaMaterials.StoneBricks),
	("Wall material 1 weight: ", 70),
	("Wall material 2", alphaMaterials.MossyStoneBricks),
	("Wall material 2 weight: ", 10),
	("Wall material 3", alphaMaterials.CrackedStoneBricks),
	("Wall material 3 weight: ", 15),
	("Wall material 4", alphaMaterials.Stone),
	("Wall material 4 weight: ", 5),
	("Walls","title"),
	),
	(
	("Use wall settings for floor", False),
	("Floor material 1", alphaMaterials.Cobblestone),
	("Floor material 1 weight: ", 65),
	("Floor material 2", alphaMaterials.MossStone),
	("Floor material 2 weight: ", 20),
	("Floor material 3", alphaMaterials.Mycelium),
	("Floor material 3 weight: ", 10),
	("Floor material 4", alphaMaterials.Gravel),
	("Floor material 4 weight: ", 5),
	("Floor","title"),
	),
	(
	("Ceiling settings",("No ceiling, just air","Use wall settings","Use settings below")),
	("Ceiling material 1", alphaMaterials.StoneBricks),
	("Ceiling material 1 weight: ", 70),
	("Ceiling material 2", alphaMaterials.MossyStoneBricks),
	("Ceiling material 2 weight: ", 10),
	("Ceiling material 3", alphaMaterials.CrackedStoneBricks),
	("Ceiling material 3 weight: ", 15),
	("Ceiling material 4", alphaMaterials.Stone),
	("Ceiling material 4 weight: ", 5),
	("Ceiling","title"),
	),
]

def perform(level, box, options):
	# Settings
	num_rnd_passages = options["Number of random passages"]
	x = options["Starting x:"]
	y = options["Starting x:"]
	rnd_seed = options["Random seed:"]

	if (rnd_seed is not ""):
		seed(rnd_seed)

	ceiling_settings = options["Ceiling settings"]
	has_ceiling = True
	ceiling_eq_wall = False
	if (ceiling_settings == "No ceiling, just air"):
		has_ceiling = False
	elif (ceiling_settings == "Use wall settings"):
		ceiling_eq_wall = True

	w = box.maxx - box.minx
	h = box.maxz - box.minz

	if (w < 3 or h < 3 or (box.maxy-box.miny < 4 and has_ceiling) or (box.maxy-box.miny < 2 and not has_ceiling)):
		#raise WindowTitle("Unable to generate maze, selection box too small.")
		print "\nERROR: Unable to generate maze, selection box too small.\n"
		return

	# (Re)define selection box (if selection is even there would probably be off by one shenanigans)
	minx = box.minx
	maxx = box.maxx
	minz = box.minz
	maxz = box.maxz
	if (w % 2 == 0):
		maxx -= 1
	if (h % 2 == 0):
		maxz -= 1

	maze_width = (w-1)/2
	maze_height = (h-1)/2

	# Setup block distributions for RNG
	wall_materials = [(options["Wall material 1"],options["Wall material 1 weight: "]), \
					  (options["Wall material 2"],options["Wall material 2 weight: "]), \
					  (options["Wall material 3"],options["Wall material 3 weight: "]), \
					  (options["Wall material 4"],options["Wall material 4 weight: "])]
	floor_materials = [(options["Floor material 1"],options["Floor material 1 weight: "]), \
					   (options["Floor material 2"],options["Floor material 2 weight: "]), \
					   (options["Floor material 3"],options["Floor material 3 weight: "]), \
					   (options["Floor material 4"],options["Floor material 4 weight: "])]
	ceiling_materials = [(options["Ceiling material 1"],options["Ceiling material 1 weight: "]), \
						 (options["Ceiling material 2"],options["Ceiling material 2 weight: "]), \
						 (options["Ceiling material 3"],options["Ceiling material 3 weight: "]), \
						 (options["Ceiling material 4"],options["Ceiling material 4 weight: "])]

	wall_blocks, wall_weights = zip(*wall_materials)
	wall_cumulative_dist = []
	for i in range(len(wall_weights)):
		wall_cumulative_dist.append(sum(wall_weights[:i+1]))

	floor_blocks = wall_blocks
	floor_cumulative_dist = wall_cumulative_dist
	ceiling_blocks = wall_blocks
	ceiling_cumulative_dist = wall_cumulative_dist
	
	if (not options["Use wall settings for floor"]):
		floor_blocks, floor_weights = zip(*floor_materials)
		floor_cumulative_dist = []
		for i in range(len(floor_weights)):
			floor_cumulative_dist.append(sum(floor_weights[:i+1]))

	if (not ceiling_eq_wall):
		ceiling_blocks, ceiling_weights = zip(*ceiling_materials)
		ceiling_cumulative_dist = []
		for i in range(len(ceiling_weights)):
			ceiling_cumulative_dist.append(sum(ceiling_weights[:i+1]))

	# Generate abstract representation of maze
	node_array = build_maze(maze_width, maze_height, num_rnd_passages, x, y)

	# Build floor
	y = box.miny
	for x in range(minx, maxx):
		for z in range(minz, maxz):
			rnd = random() * floor_cumulative_dist[-1]
			block = floor_blocks[bisect(floor_cumulative_dist, rnd)]
			level.setBlockAt(x,y,z, block.ID)
			level.setBlockDataAt(x,y,z, block.blockData)

	# Build walls (extrapolate node_array in chunks of 4 blocks per node)
	upto_y = box.maxy
	if (has_ceiling):
		upto_y -=1

	for y in range(box.miny+1, upto_y):
		for w in range(0,maze_width):
			base_x = minx + w*2
			for h in range(0,maze_height):
				base_z = minz + h*2

				# corner wall block
				rnd = random() * wall_cumulative_dist[-1]
				block = wall_blocks[bisect(wall_cumulative_dist, rnd)]
				level.setBlockAt(base_x,y,base_z, block.ID)
				level.setBlockDataAt(base_x,y,base_z, block.blockData)

				# up wall/passage
				if ( (node_array[h][w] & 0b0010) == 0 ):
					rnd = random() * wall_cumulative_dist[-1]
					block = wall_blocks[bisect(wall_cumulative_dist, rnd)]
					level.setBlockAt(base_x+1,y,base_z, block.ID)
					level.setBlockDataAt(base_x+1,y,base_z, block.blockData)
				else:
					level.setBlockAt(base_x+1,y,base_z, 0)
					level.setBlockDataAt(base_x+1,y,base_z, 0)

				# left wall/passage
				if ( (node_array[h][w] & 0b0001) == 0 ):
					rnd = random() * wall_cumulative_dist[-1]
					block = wall_blocks[bisect(wall_cumulative_dist, rnd)]
					level.setBlockAt(base_x,y,base_z+1, block.ID)
					level.setBlockDataAt(base_x,y,base_z+1, block.blockData)
				else:
					level.setBlockAt(base_x,y,base_z+1, 0)
					level.setBlockDataAt(base_x,y,base_z+1, 0)

				# corner empty block
				level.setBlockAt(base_x+1,y,base_z+1, 0)
				level.setBlockDataAt(base_x+1,y,base_z+1, 0)

			# cap row
			rnd = random() * wall_cumulative_dist[-1]
			block = wall_blocks[bisect(wall_cumulative_dist, rnd)]
			level.setBlockAt(base_x,y,maxz-1, block.ID)
			level.setBlockDataAt(base_x,y,maxz-1, block.blockData)

			rnd = random() * wall_cumulative_dist[-1]
			block = wall_blocks[bisect(wall_cumulative_dist, rnd)]
			level.setBlockAt(base_x+1,y,maxz-1, block.ID)
			level.setBlockDataAt(base_x+1,y,maxz-1, block.blockData)	

		# cap level
		for z in range(minz, maxz):
			rnd = random() * wall_cumulative_dist[-1]
			block = wall_blocks[bisect(wall_cumulative_dist, rnd)]
			level.setBlockAt(maxx-1,y,z, block.ID)
			level.setBlockDataAt(maxx-1,y,z, block.blockData)

	# Build ceiling
	if (has_ceiling):
		y = box.maxy-1
		for x in range(minx, maxx):
			for z in range(minz, maxz):
				rnd = random() * ceiling_cumulative_dist[-1]
				block = ceiling_blocks[bisect(ceiling_cumulative_dist, rnd)]
				level.setBlockAt(x,y,z, block.ID)
				level.setBlockDataAt(x,y,z, block.blockData)

	level.markDirtyBox(box)