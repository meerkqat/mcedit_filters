from random import randint, shuffle

"""
Script for generating ore in MCEdit.
The algorithm tries to emulate what Minecraft does when generating ores.
In a nutshell it looks at how many stone blocks are in a chunk layer (16 blocks vertically) and which chunk layer it is.
It then generates ores by replacing a percentage of those stone blocks. 
What the percentage for a specific ore at a given depth is can be seen in layer_ore_percentage array bellow.
So if you want a custom percentage of a ceratin ore you can modify that. To get more (or less) emeralds to spawn you 
can edit this line (it's almost at the very bottom):
	num_emeralds += randint(2,6)
randint(a,b) selects a random number between a & b (inclusively), so to get more emeralds try something like:
	num_emeralds += randint(5,10)
and it will add a random amount (from 5 to 10) of  them for each selected chunk. 
"""

# Adjust this to get custom percentages of ores generated
# The defults values are more or less what the Minecraft Wiki says is the generation 
# rate for each ore. If no data was avaliable I just guessed at the approximate value.
layer_ore_percentage = [
#  Coal,  Iron,  Gold,   Lapis, Redstone, Diamonds
(1.0450, 0.7200, 0.1437, 0.0650, 0.9750, 0.0846), # layers  0-15
(1.1000, 0.7200, 0.1437, 0.0650, 0.0000, 0.0000), # layers 16-31
(1.1000, 0.7200, 0.0000, 0.0000, 0.0000, 0.0000), # layers 32-47
(1.0000, 0.5900, 0.0000, 0.0000, 0.0000, 0.0000), # layers 48-63
(0.8500, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000), # layers 64-79
(0.4500, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000), # layers 80-95
(0.0200, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000)  # layers 96-111 and up
]

ores = [16,15,14,21,73,56]

# returns the number of each ore to place in specified level (chunk layer) based on the number of stone blocks in it
def calc_ores_for_layer(lv, num_stones):
	if (lv > 7): # anything over layer 111 uses layer 111's percentages
		lv = 7
	ore_count = []
	for ore_index in range(6):
		ore_count.append(int((num_stones * layer_ore_percentage[lv%7][ore_index])/100))
	
	return ore_count
	
# tries to place a bunch of some ore around x,y,z
# returns how many ores from bunch it could not place
# min_y is the bottom limit (inclusive) of level (chunk layer), max_y the top limit (exclusive) 
#	-> layers allowed to place ores in is on interval [min_y,max_y)
def place_ore(level, ore, x, y, z, min_y, max_y, bunch):
	if(bunch == 0):
		return 0
	
	level.setBlockAt(x, y, z, ores[ore])
	num_not_placed = bunch-1
	
	# this is required so ores don't just generate in a straight line
	directions = [(0,-1,0),(0,1,0),(1,0,0),(-1,0,0),(0,0,1),(0,0,-1),(1,1,0),(1,-1,0),(-1,1,0),(-1,-1,0),(1,0,1),(1,0,-1),(-1,0,1),(-1,0,-1),(0,1,1),(0,1,-1),(0,-1,1),(0,-1,-1)]
	shuffle(directions)
	
	for (dx,dy,dz) in directions:
		if(level.blockAt(x+dx,y+dy,z+dz) == 1 and y+dy < max_y and y+dy >= min_y): # if selected direction is stone within the bounds of the layer
			num_not_placed = place_ore(level, ore, x+dx, y+dy, z+dz, max_y, min_y, num_not_placed)
			if (num_not_placed == 0):
				break	
	
	return num_not_placed
	
	
displayName = "Ore Generator"

inputs = (
	("Include emeralds:", False),
)

def perform(level, box, options):
	emeralds_included = options["Include emeralds:"]
	
	min_lv = box.miny/16 # level the bottom side of the box is at (corresponds with layer_ore_percentage)
	stone_count = [0]*(box.maxy/16 - box.miny/16 + 1)
	
	max_lv = min_lv # max_lv treated as curr_lv in next for loop; becomes max_lv at the end of loop
	for y in xrange(box.miny, box.maxy):
		max_lv = y/16
		for x in xrange(box.minx, box.maxx):
				for z in xrange(box.minz, box.maxz):
					if(level.blockAt(x, y, z) == 1):
						stone_count[max_lv-min_lv] += 1
	
	# normal ore generation
	
	# lv is relative to the selection box
	# the absolute level is min_lv + lv
	for lv in range(max_lv):
		#for every level calc how many ores to place
		ores_in_layer = calc_ores_for_layer(min_lv + lv, stone_count[lv])
		#print ores_in_layer
		for ore in range(6):
			num_placed = 0
			while (ores_in_layer[ore] > 0):
				# get how big of a group of ores to place
				bunch = randint(1,10) # 1-10 I feel is an ok generalization
				bunch = min(bunch, ores_in_layer[ore])
				if(ore == 0): # coal forms bigger groups the further down it is
					if(min_lv+lv < 3): 
						bunch = randint(18,25)
						bunch = min(bunch, ores_in_layer[ore])
					elif(min_lv+lv < 4): # sea level @ approximately lv 4
						bunch = randint(15,30)
						bunch = min(bunch, ores_in_layer[ore])
					elif(min_lv+lv < 5):
						bunch = randint(8,20)
						bunch = min(bunch, ores_in_layer[ore])
					else:
						bunch = randint(1,9)
						bunch = min(bunch, ores_in_layer[ore])
						
				bottom_lim = (min_lv+lv)*16
				
				# get a random stone block
				x = randint(box.minx, box.maxx)
				y = randint(bottom_lim,bottom_lim+15)
				z = randint(box.minz, box.maxz)
				while (level.blockAt(x,y,z) != 1):
					x = randint(box.minx, box.maxx)
					y = randint(bottom_lim,bottom_lim+15)
					z = randint(box.minz, box.maxz)
					
				# place up to "bunch" of ores
				num_not_placed = place_ore(level,ore,x,y,z,bottom_lim,bottom_lim+16,bunch)
				# update ores left to place count & number of stones left (for emerald generation)
				ores_in_layer[ore] = ores_in_layer[ore] - (bunch - num_not_placed)
				stone_count[lv] = stone_count[lv] - (bunch - num_not_placed)
	
	# emerald ore generation
	# emerald generation is slightly different; 3-8 of them spawn in each chunk in the bottom 2 chunk layers
	if(emeralds_included):
		# h = how many of the 2 bottom-most chunks are selected
		h = 0
		if(min_lv == 0 and max_lv > 1):
			h = 2
		elif(min_lv == max_lv and max_lv < 2):
			h = 1
		chunks = (box.maxx-box.minx)*(box.maxz-box.minz)/256 # how many chunks are selected
		
		# for each of the selected bottom 2 chunk layers
		for lv in range(h):
			num_emeralds = 0
			for ch in range(chunks): # get a random number of emeralds to place in it
				num_emeralds += randint(2,6) # 3-8 looks a bit much compared to reality

			num_emeralds = min(num_emeralds, stone_count[lv]) # make sure there's enough stone to replace
			
			bottom_lim = (min_lv+lv)*16
			for e in range(num_emeralds): # place all the emeralds in the layer at random positions
				x = randint(box.minx, box.maxx)
				y = randint(bottom_lim,bottom_lim+15)
				z = randint(box.minz, box.maxz)
				while (level.blockAt(x,y,z) != 1):
					x = randint(box.minx, box.maxx)
					y = randint(bottom_lim,bottom_lim+15)
					z = randint(box.minz, box.maxz)
				
				level.setBlockAt(x, y, z, 129)
	
	#chunk changed
	for chunk, slices, point in level.getChunkSlices(box):				
		chunk.chunkChanged()
	
	
	
	
	
	