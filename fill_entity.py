from pymclevel import TAG_Compound
from pymclevel import TAG_Int
from pymclevel import TAG_Short
from pymclevel import TAG_Byte
from pymclevel import TAG_String
from pymclevel import TAG_Float
from pymclevel import TAG_Double
from pymclevel import TAG_List

"""
A filter that can create entities similar to how the fill tool can create blocks.
Also note: some entities (creepers, skeletons etc.) are 2 blocks high and the entity block (red outline) should be placed where the head will be;
i.e. it's easy to place them half sunken into the ground.

I didn't include item entities, though that can be easily added by appending the appropriate entity Savegame IDs. 
At the time of writing this, they can be found on the Minecraft wiki (http://www.minecraftwiki.net/wiki/Data_values#Entity_IDs)
"""

entities = ("Creeper", "Skeleton", "WitherSkeleton", "Spider", "Giant", "Zombie", "Slime", "Ghast", "PigZombie", "Enderman", \
	"CaveSpider", "Silverfish", "Blaze", "LavaSlime" , "EnderDragon", "WitherBoss", "Witch", "Bat", "Pig", \
	"Sheep", "Cow", "Chicken", "Squid", "Wolf", "MushroomCow", "SnowMan", "Ozelot", "VillagerGolem", "Villager", "EnderCrystal")

AIR = 300 # current amount of air
FALL = 0 # current fall distance
ANGRY = 0 # only works with some mobs e.g. zombie pigmen

inputs = (
	("Entity id", entities),
	("Health", (5, 0, 32767)),
	("Can pick up loot", False),
	("Invulnerable", False),
	("Is on fire", False),
	("Persistent", False),
	("Velocity in x", 0.0),
	("Velocity in y", 0.0),
	("Velocity in z", 0.0),
	("Fine tune x position", 0.0),
	("Fine tune y position", 0.0),
	("Fine tune z position", 0.0),
	#("Owner",("string","value=")),
	("Place in air blocks only", True)
)

displayName = "Fill Entities"


def perform(level, box, options):
	eid = options["Entity id"]
	hp = options["Health"]
	fired_up = -1
	if options["Is on fire"]:
		fired_up = 1
	can_loot = 0
	if options["Can pick up loot"]:
		can_loot = 1
	invulnerable = 0
	if options["Invulnerable"]:
		invulnerable = 1
	mov_x = options["Velocity in x"]
	mov_y = options["Velocity in y"]
	mov_z = options["Velocity in z"]
	fine_x = options["Fine tune x position"]
	fine_y = options["Fine tune y position"]
	fine_z = options["Fine tune z position"]
	#owner = options["Owner"]
	air_only = options["Place in air blocks only"]
	persist = 0
	if (options["Persistent"]):
		persist = 1

	skeleton = 0
	if eid == "WitherSkeleton":
		skeleton = 1
		eid = "Skeleton"

	for pos_x in range(box.minx, box.maxx):
		for pos_y in range(box.miny, box.maxy):
			for pos_z in range(box.minz, box.maxz):
				if (level.blockAt(pos_x, pos_y, pos_z) != 0 and air_only):
					continue
				grounded = 0
				if(level.blockAt(pos_x, pos_y-1, pos_z) != 0 or level.blockAt(pos_x, pos_y-2, pos_z) != 0):
					grounded = 1

				entity = TAG_Compound()
				entity["CanPickUpLoot"] = TAG_Byte(can_loot)
				entity["Invulnerable"] = TAG_Byte(invulnerable)
				entity["OnGround"] = TAG_Byte(grounded)
				entity["PersistenceRequired"] = TAG_Byte(persist)
				entity["Angry"] = TAG_Byte(ANGRY)
				if eid == "Skeleton":
					entity["SkeletonType"] = TAG_Byte(skeleton)
				elif eid == "Bat":
					entity["BatFlags"] = TAG_Byte(1)
				entity["Air"] = TAG_Short(AIR)
				entity["Fire"] = TAG_Short(fired_up)
				entity["Health"] = TAG_Short(hp)
				entity["FallDistance"] = TAG_Float(FALL)
				entity["id"] = TAG_String(eid)
				#entity["Owner"] = TAG_String(owner)
				entity["Motion"] = TAG_List()
				entity["Motion"].append(TAG_Double(mov_x))
				entity["Motion"].append(TAG_Double(mov_y))
				entity["Motion"].append(TAG_Double(mov_z))
				entity["Pos"] = TAG_List()
				entity["Pos"].append(TAG_Double(pos_x + 0.5 + fine_x))
				entity["Pos"].append(TAG_Double(pos_y + 0.5 + fine_y))
				entity["Pos"].append(TAG_Double(pos_z + 0.5 + fine_z))
				entity["Rotation"] = TAG_List()
				entity["Rotation"].append(TAG_Float(0))
				entity["Rotation"].append(TAG_Float(0))

				chunk = level.getChunk(pos_x/16, pos_z/16)
				chunk.Entities.append(entity)
				chunk.dirty = True