from pymclevel import TAG_Compound
from pymclevel import TAG_Int
from pymclevel import TAG_Short
from pymclevel import TAG_Byte
from pymclevel import TAG_String
from pymclevel import TAG_Float
from pymclevel import TAG_Double
from pymclevel import TAG_List
from pymclevel import TileEntity

# Mines out all the blocks in the selected area and places them in chests. 
# Takes into account stack size, unobtainable blocks, and you can specifiy wether it should "use" a silk touch tool.
# Kinda cheaty, but you can also specify if it should pick up spawners and cake (which should otherwise always break when mined).

# Should work for all blocks in Minecraft 1.5 
# The problem is with blocks that don't drop themselves at all in-game, i.e. doors and the like. 
# These require special handling - that's what the giant "transf" dictionary in perform() is.

# todo? kinda derpy for paintings, item frames, minecarts etc. since they aren't tile entities or actual blocks

displayName = "Excavate"

# if silk touch == false all the stone will be converted into cobble, leaves will get destroyed etc.
# if use buckets == false lava & water blocks will get destroyed, otherwise source blocks will get "collected" into buckets
inputs = (
	("Silk Touch", True),
	("Use buckets", True),
	("Pick up spawners", False),
	("Pick up cakes", ("No","Yes, all","Yes, only whole")),
)

# create a new chest & chest tile entity @ x,y,z
def genChest(x, y, z, level):
	level.setBlockAt(x,y,z, 54)
	chest = TileEntity.Create("Chest")
	TileEntity.setpos(chest, (x, y, z))
	chest["x"] = TAG_Int(x)
	chest["y"] = TAG_Int(y)
	chest["z"] = TAG_Int(z)
	chest["Items"] = TAG_List()
	level.getChunk(x / 16, z / 16).TileEntities.append(chest)

# place n blocks of type block:data into chest @ coords x,y,z in inventory position pos
def placeInChest(x,y,z,pos,block,data,n,level):
	chest = level.tileEntityAt(x, y, z)

	item = TAG_Compound()					
	item["id"] = TAG_Short(block)
	item["Damage"] = TAG_Short(data)
	item["Count"] = TAG_Byte(n)
	item["Slot"] = TAG_Byte(pos)

	chest["Items"].append(item)

# empties inventory of tile entity tent (if it has an inventory) and places the items in a dictionary called blocks. 
# returns the dictionary.
def emptyContainer(tent, blocks):
	try:
		for item in tent["Items"]:
			block = item["id"].value
			data = item["Damage"].value
			n = item["Count"].value
			try:
				blocks[block,data] += 1
			except:
				blocks[block,data] = 1
	except:
		# not a container tile entity
		pass

		return blocks

# place items from dictionary itemdict in chests  starting with a chest @ start_x,start_y,start_z and inventory position start_pos
def fillChests(start_x, start_y, start_z, start_pos, itemdict, level, box):
	if itemdict == None:
		return (start_x, start_y, start_z, start_pos)

	#todo? not all stack to 64 (not really a problem)
	stack_size = 64
	for (block,data) in itemdict:
		count = itemdict[(block,data)]
		while count > 0:
			n = count
			
			if n > stack_size:
				n = stack_size
			count -= stack_size

			placeInChest(start_x,start_y,start_z,start_pos,block,data,n,level)
			start_pos += 1

			# todo? the chest layout could probably be done more efficiently
			# currently all chests are separated by 1 block space (no double chests)
			if start_pos == 27: # if inventory is filled up
				start_pos = 0
				start_x += 2
				if start_x > box.maxx-1: # next row
					start_x = box.minx
					start_z += 2
					if start_z > box.maxz-1: # next layer
						start_z = box.minz
						start_y += 1
				genChest(start_x, start_y, start_z, level)

	return (start_x, start_y, start_z, start_pos)

def perform(level, box, options):
	use_silk = options["Silk Touch"]
	use_buckets = options["Use buckets"]
	keep_spawners = options["Pick up spawners"]
	keep_cake = options["Pick up cakes"]

	# transformation dictionaries; format: { (id,damage):(id,damage) } -> { from:into }
	# default tramsformations, always happens (unobtainble blocks)
	transf = {(8,0):(0,0),(8,1):(0,0),(8,2):(0,0),(8,3):(0,0),(8,4):(0,0),(8,5):(0,0),(8,6):(0,0),(8,7):(0,0),(8,8):(0,0),(8,9):(0,0), \
	(8,10):(0,0),(8,11):(0,0),(8,12):(0,0),(8,13):(0,0),(8,14):(0,0),(10,0):(0,0),(10,1):(0,0),(10,2):(0,0),(10,3):(0,0),(10,4):(0,0), \
	(10,5):(0,0),(10,6):(0,0),(10,7):(0,0),(10,8):(0,0),(10,9):(0,0),(10,10):(0,0),(10,11):(0,0),(10,12):(0,0),(10,13):(0,0), \
	(10,14):(0,0),(23,1):(23,0),(23,2):(23,0),(23,3):(23,0),(23,4):(23,0),(23,5):(23,0),(26,0):(355,0),(26,1):(355,0), \
	(26,2):(355,0),(26,3):(355,0),(26,8):(0,0),(26,9):(0,0),(26,10):(0,0),(26,11):(0,0),(29,1):(29,0),(29,2):(29,0),(29,3):(29,0), \
	(29,4):(29,0),(29,5):(29,0),(29,8):(29,0),(29,9):(29,0),(29,10):(29,0),(29,11):(29,0),(29,12):(29,0),(29,13):(29,0),(33,1):(33,0), \
	(33,2):(33,0),(33,3):(33,0),(33,4):(33,0),(33,5):(33,0),(33,8):(33,0),(33,9):(33,0),(33,10):(33,0),(33,11):(33,0),(33,12):(33,0), \
	(33,13):(33,0),(34,1):(0,0),(34,8):(0,0),(34,9):(0,0),(34,10):(0,0),(34,11):(0,0),(34,12):(0,0),(34,13):(0,0),(36,0):(0,0),(50,1):(50,0), \
	(50,2):(50,0),(50,3):(50,0),(50,4):(50,0),(51,0):(0,0),(52,0):(0,0),(54,2):(54,0),(54,3):(54,0),(54,4):(54,0),(54,5):(54,0), \
	(55,0):(331,0),(59,0):(295,0),(60,0):(3,0),(61,2):(61,0),(61,3):(61,0),(61,4):(61,0),(61,5):(61,0),(62,2):(61,0),(62,3):(61,0), \
	(62,4):(61,0),(62,5):(61,0),(63,0):(323,0),(63,1):(323,0),(63,2):(323,0),(63,3):(323,0),(63,4):(323,0),(63,5):(323,0),(63,6):(323,0), \
	(63,7):(323,0),(63,8):(323,0),(63,9):(323,0),(63,10):(323,0),(63,11):(323,0),(63,12):(323,0),(63,13):(323,0),(63,14):(323,0), \
	(63,15):(323,0),(64,0):(0,0),(64,1):(0,0),(64,2):(0,0),(64,3):(0,0),(64,4):(0,0),(64,5):(0,0),(64,6):(0,0),(64,7):(0,0),(64,8):(324,0), \
	(64,9):(324,0),(65,2):(65,0),(65,3):(65,0),(65,4):(65,0),(65,5):(65,0),(27,1):(27,0),(27,6):(27,0),(27,7):(27,0),(27,8):(27,0),(27,9):(27,0), \
	(28,1):(28,0),(28,6):(28,0),(28,7):(28,0),(28,8):(28,0),(28,9):(28,0),(66,1):(66,0),(66,6):(66,0),(66,7):(66,0),(66,8):(66,0),(66,9):(66,0), \
	(157,1):(157,0),(157,6):(157,0),(157,7):(157,0),(157,8):(157,0),(157,9):(157,0),(68,2):(323,0),(68,3):(323,0),(68,4):(323,0),(68,5):(323,0), \
	(71,0):(0,0),(71,1):(0,0),(71,2):(0,0),(71,3):(0,0),(71,4):(0,0),(71,5):(0,0),(71,6):(0,0),(71,7):(0,0),(71,8):(330,0),(71,9):(330,0), \
	(74,0):(0,0),(75,1):(76,0),(75,2):(76,0),(75,3):(76,0),(75,4):(76,0),(75,5):(76,0),(76,1):(76,0),(76,2):(76,0),(76,3):(76,0),(76,4):(76,0), \
	(76,5):(76,0),(78,0):(332,0),(78,1):(332,0),(78,2):(332,0),(78,3):(332,0),(78,4):(332,0),(78,5):(332,0),(78,6):(332,0),(78,7):(332,0), \
	(83,0):(338,0),(90,0):(0,0),(92,0):(0,0),(92,1):(0,0),(92,2):(0,0),(92,3):(0,0),(92,4):(0,0),(92,5):(0,0),(93,0):(356,0), \
	(93,1):(356,0),(93,2):(356,0),(93,3):(356,0),(93,4):(356,0),(93,5):(356,0),(93,6):(356,0),(93,7):(356,0),(93,8):(356,0),(93,9):(356,0), \
	(93,10):(356,0),(93,11):(356,0),(93,12):(356,0),(93,13):(356,0),(93,14):(356,0),(93,15):(356,0),(94,0):(356,0),(94,1):(356,0),(94,2):(356,0), \
	(94,3):(356,0),(94,4):(356,0),(94,5):(356,0),(94,6):(356,0),(94,7):(356,0),(94,8):(356,0),(94,9):(356,0),(94,10):(356,0),(94,11):(356,0), \
	(94,12):(356,0),(94,13):(356,0),(94,14):(356,0),(94,15):(356,0),(96,1):(96,0),(96,2):(96,0),(96,3):(96,0),(96,4):(96,0),(96,5):(96,0), \
	(96,6):(96,0),(96,7):(96,0),(96,8):(96,0),(96,9):(96,0),(96,10):(96,0),(96,11):(96,0),(96,12):(96,0),(96,13):(96,0),(96,14):(96,0), \
	(96,15):(96,0),(95,0):(0,0),(104,0):(361,0),(105,0):(362,0),(106,1):(106,0),(106,2):(106,0),(106,4):(106,0),(106,8):(106,0),(107,1):(107,0), \
	(107,2):(107,0),(107,3):(107,0),(107,4):(107,0),(107,5):(107,0),(107,6):(107,0),(107,7):(107,0),(115,0):(372,0),(117,0):(379,0),(118,0):(0,0), \
	(119,0):(0,0),(124,0):(123,0),(127,0):(351,3),(130,2):(130,0),(130,3):(130,0),(130,4):(130,0),(130,5):(130,0),(132,0):(287,0),(140,0):(0,0), \
	(141,0):(295,0),(142,0):(295,0),(149,0):(404,0),(149,1):(404,0),(149,2):(404,0),(149,3):(404,0),(149,4):(404,0),(149,5):(404,0), \
	(149,6):(404,0),(149,7):(404,0),(149,8):(404,0),(149,9):(404,0),(149,10):(404,0),(149,11):(404,0),(149,12):(404,0),(149,13):(404,0), \
	(149,14):(404,0),(149,15):(404,0),(150,0):(404,0),(150,1):(404,0),(150,2):(404,0),(150,3):(404,0),(150,4):(404,0),(150,5):(404,0), \
	(150,6):(404,0),(150,7):(404,0),(150,8):(404,0),(150,9):(404,0),(150,10):(404,0),(150,11):(404,0),(150,12):(404,0),(150,13):(404,0), \
	(150,14):(404,0),(150,15):(404,0),(154,2):(154,0),(154,3):(154,0),(154,4):(154,0),(154,5):(154,0),(155,3):(155,2),(155,4):(155,2), \
	(158,1):(158,0),(158,2):(158,0),(158,3):(158,0),(158,4):(158,0),(158,5):(158,0)}
	# silk touch transformations (silky->non-silky)
	silk_transf = {(1,0):(4,0),(2,0):(3,0),(18,0):(0,0),(18,1):(0,0),(18,2):(0,0),(18,3):(0,0),(20,0):(0,0),(31,0):(0,0),(31,1):(0,0), \
	(31,2):(0,0),(31,3):(0,0),(32,0):(0,0),(79,0):(9,0),(80,0):(332,0),(102,0):(0,0),(106,0):(0,0),(110,0):(3,0),(99,0):(39,0),(100):(40,0)}

	# block dictionaries; format: { (id,damage):numberOfItems }
	blocks = {}
	container_items = {}

	# find all the blocks in the selection
	for x in xrange(box.minx, box.maxx):
		for y in xrange(box.miny, box.maxy):
			for z in xrange(box.minz, box.maxz):
				# leave bedrock as is
				if (level.blockAt(x,y,z) == 7):
					continue

				chunk = level.getChunk(x/16, z/16)

				# get block @ x,y,z and remove it
				try:
					blocks[level.blockAt(x,y,z),level.blockDataAt(x,y,z)] += 1
				except:
					blocks[level.blockAt(x,y,z),level.blockDataAt(x,y,z)] = 1

				level.setBlockAt(x,y,z, 0)
				level.setBlockDataAt(x,y,z, 0)

				# handle tile entities e.g. chests, dispensers...
				# save items in their inventories to a separate dictionary 
				# (we dont want any of them accidentally getting deleted by the silk touch, unobtainable or buckets filtering later on)
				te = level.tileEntityAt(x,y,z)
				if te != None:
					container_items = emptyContainer(te,container_items)
					chunk.TileEntities.remove(te)

				chunk.dirty = True

	if keep_spawners:
		transf.pop((52,0))
	if keep_cake == "Yes, all" or keep_cake == "Yes, only whole":
		transf[(92,0)] = (354,0)
	if keep_cake == "Yes, all":
		transf[(92,1)] = (354,0)
		transf[(92,2)] = (354,0)
		transf[(92,3)] = (354,0)
		transf[(92,4)] = (354,0)
		transf[(92,5)] = (354,0)

	# handle unobtainable blocks
	for unobtainable in transf:
		n = blocks.get(unobtainable) # test if we picked up an unobtainable block
		if n != None:
			into = transf[unobtainable]
			try:
				blocks[into] += blocks[unobtainable]
			except:
				blocks[into] = blocks[unobtainable]
			blocks.pop(unobtainable)

	# handle silk touch
	if not use_silk:
		for silky in silk_transf:
			n = blocks.get(silky) # test if we picked up "silky" block
			if n != None:
				into = silk_transf[silky]
				try:
					blocks[into] += blocks[silky]
				except:
					blocks[into] = blocks[silky]
				blocks.pop(silky)

	# handle water & lava
	n = blocks.get((9,0))
	if n != None:
		if use_buckets:
			blocks[(326,0)] = n
		blocks.pop((9,0))

	for dmg in range(0,15):
		n = blocks.get((11,dmg))
		if n != None:
			if use_buckets:
				blocks[(327,0)] = n
			blocks.pop((11,dmg))
	
	# remove any air blocks 
	try:
		blocks.pop((0,0))
	except:
		pass

	# initialize the first chest
	curr_x = box.minx
	curr_y = box.miny
	curr_z = box.minz
	inv_pos = 0
	genChest(curr_x, curr_y, curr_z, level)
	
	# place all the blocks in chests
	(curr_x, curr_y, curr_z, inv_pos) = fillChests(curr_x, curr_y, curr_z, inv_pos, blocks, level, box)
	(curr_x, curr_y, curr_z, inv_pos) = fillChests(curr_x, curr_y, curr_z, inv_pos, container_items, level, box)

	# delete last chest if empty
	chest = level.tileEntityAt(curr_x, curr_y, curr_z)
	if len(chest["Items"]) == 0:
		level.setBlockAt(curr_x, curr_y, curr_z, 0)
		chunk = level.getChunk(curr_x/16, curr_z/16)
		chunk.TileEntities.remove(chest)
