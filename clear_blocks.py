from pymclevel import alphaMaterials

# Deletes blocks in a selected area that have 3 or more faces exposed to air (or water or lava).
# This can sort of declutter, clean or straighten edges in caves & tunnels.


#these blocks are treated as air (water & lava are appended later); 0 (air) must always remain in array
airBlocks = [0]

displayName = "Clear out Blocks"

def numSidesExposed(level,x,y,z):
	surroundLiquid = 0
	surroundWater = 0
	surroundLava = 0
	nExpose = 0
	#X
	b = level.blockAt(x+1, y, z)
	if(b in airBlocks):
		if(b == 8 or b == 9):
			surroundWater += 1
		elif(b == 10 or b == 11):
			surroundLava += 1
		nExpose += 1
	b = level.blockAt(x-1, y, z)
	if(b in airBlocks):
		if(b == 8 or b == 9):
			surroundWater += 1
		elif(b == 10 or b == 11):
			surroundLava += 1
		nExpose += 1
		
	#Z
	b = level.blockAt(x, y, z+1)
	if(b in airBlocks):
		if(b == 8 or b == 9):
			surroundWater += 1
		elif(b == 10 or b == 11):
			surroundLava += 1
		nExpose += 1
	b = level.blockAt(x, y, z-1)
	if(level.blockAt(x, y, z-1) in airBlocks):
		if(b == 8 or b == 9):
			surroundWater += 1
		elif(b == 10 or b == 11):
			surroundLava += 1
		nExpose += 1
		
	#Y
	if(level.blockAt(x, y+1, z) in airBlocks):
		nExpose += 1
	if(level.blockAt(x, y-1, z) in airBlocks):
		nExpose += 1
		
	if (surroundLava > surroundWater):
		surroundLiquid = 11
	if ((surroundLava <= surroundWater) and (surroundWater != 0)):
		surroundLiquid = 9
		
	return nExpose,surroundLiquid

inputs = (
	("Clear:", alphaMaterials.Stone),
	("Delete all", False),
	("Number of passes:", (1,1,100)),
	("Treat water as air", True),
	("Treat lava as air", True),
)

def perform(level, box, options):
	delThis = options["Clear:"]
	delAll = options["Delete all"]
	npass = options["Number of passes:"]
	waterIncluded = options["Treat water as air"]
	lavaIncluded = options["Treat lava as air"]
	
	if(waterIncluded):
		airBlocks.append(8)
		airBlocks.append(9)
	if(lavaIncluded):
		airBlocks.append(10)
		airBlocks.append(11)
		
	width = box.maxx - box.minx
	height = box.maxy - box.miny
	depth = box.maxz - box.minz
	
	destroyThese = []
	
	for np in xrange(npass):
		for x in xrange(width):
			for y in xrange(height):
				for z in xrange(depth):
					b = level.blockAt(box.minx + x, box.miny + y, box.minz + z)
					nExpose,filling = numSidesExposed(level, box.minx + x, box.miny + y, box.minz + z)
					if((b == delThis.ID or delAll) and (nExpose >= 3) and (b not in airBlocks)):
						info = [box.minx + x, box.miny + y, box.minz + z, filling]
						destroyThese.append(info)
		
		# destroying blocks requires a separate for loop
		# if you were to destroy while you go, ceratin blocks would get uncovered and perhaps destroyed too
		# (those should get destroyed only on the next pass)
		for block in destroyThese:	
			print block
			level.setBlockAt(block[0], block[1], block[2], block[3])
			level.setBlockDataAt(block[0], block[1], block[2], 0)	
			
	# update chunk
	for chunk, slices, point in level.getChunkSlices(box): 
		chunk.chunkChanged()